﻿using System.Web;
using System.Web.Mvc;

namespace ASC_Dev_WebAPI_Do_Not_Use_Wa
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
