using System;
using System.Reflection;

namespace ASC_Dev_WebAPI_Do_Not_Use_Wa.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}